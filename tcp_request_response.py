import socket
import logging
import time

DEFAULT_IP = 'localhost'
DEFAULT_PORT = 9989


class Command(object):
    def __init__(self, command, lnk=None, cid=0, no_reply=False):
        self.command = command
        self.no_reply = no_reply
        self.sent = False
        self.cid = cid
        self.reply = ''
        self.link = lnk
        self.wait_times = 2

    def __str__(self):
        return 'command: {} reply: {}\nlink:\n{}'.format(self.command, self.reply, self.link)


class TcpRequestResponseClient(object):

    def __init__(self, pollintervall=1, delimiter=b'\n'):
        self.command_que = []
        self.pollintervall = pollintervall
        self._socket = None
        self.host = None
        self.port = None
        self.timer = time.monotonic()
        self._on_overload = None
        self._on_message = None
        self._on_message_sent = None
        self._on_no_reply = None
        self._on_available = None
        self._on_error = None
        self.delimiter = delimiter
        self._disconnect = False
        self.last_loop_send = True
        self.run_loop = True

    def connect(self, host=DEFAULT_IP, port=DEFAULT_PORT):
        if len(host) == 0:
            raise ValueError('Invalid host.')
        if port <= 0:
            raise ValueError('Invalid port number.')
        self.host = host
        self.port = port
        logging.info('Trying to connect to {}'.format(self.host))
        return self.reconnect()

    def reconnect(self):
        s = None
        for res in socket.getaddrinfo(self.host, self.port, socket.AF_INET, socket.SOCK_STREAM):
            af, socktype, proto, canonname, sa = res
            try:
                s = socket.socket(af, socktype, proto)
            except socket.error as err:
                s = None
                logging.error('TcpRequestResponseClient.connect: {}'.format(err))
                continue
            try:
                s.connect(sa)
            except socket.error as err:
                s.close()
                logging.error('TcpRequestResponseClient.connect: {}'.format(err))
                s = None
                continue
            break
        if s is None:
            logging.error('TcpRequestResponseClient.connect: Could not open socket')
            return None
        s.setblocking(False)
        self._socket = s
        logging.info('Connected to {} on port {}'.format(self.host, self.port))
        return s

    def disconnect(self):
        logging.info('Disconnecting {} on port {}'.format(self.host, self.port))
        self._disconnect = True
        self.close()

    @property
    def on_message(self):
        return self._on_message

    @on_message.setter
    def on_message(self, func):
        self._on_message = func

    @property
    def on_message_sent(self):
        return self._on_message_sent

    @on_message_sent.setter
    def on_message_sent(self, func):
        self._on_message_sent = func

    @property
    def on_no_reply(self):
        return self._on_no_reply

    @on_no_reply.setter
    def on_no_reply(self, func):
        self._on_no_reply = func

    @property
    def on_available(self):
        return self._on_available

    @on_available.setter
    def on_available(self, func):
        self._on_available = func

    @property
    def on_overload(self):
        return self._on_overload

    @on_overload.setter
    def on_overload(self, func):
        self._on_overload = func

    def send(self):
        if self.command_que:
            if time.monotonic() - self.timer > self.pollintervall:
                if len(self.command_que) > 1 and self.on_overload:
                    if self.on_overload:
                        self.on_overload()
                    logging.error('polling too fast, ' + str(len(self.command_que)) + ' commands waiting.')
                command = self.command_que[0]
                if command.sent:
                    if self._on_no_reply:
                        self.on_no_reply(command)
                    logging.warning('No reply from: <{}>'.format(command.command))
                    if command.wait_times <= 0:
                        self.command_que.pop(0)
                        logging.warning('No reply from: <{}> giving up'.format(command.command))
                        return
                    else:
                        logging.warning('No reply from: <{}> retrying'.format(command.command))
                        command.wait_times -= 1
                cmd_to_send = command.command
                if not cmd_to_send.endswith(self.delimiter):
                    cmd_to_send += self.delimiter
                self._socket.send(cmd_to_send)
                command.sent = True
                if self.on_available:
                    if len(self.command_que) < 1:
                        self.on_available()
                    else:
                        logging.debug('empty command que')
                else:
                    logging.debug('on_available callback not set')
                if self.on_message_sent:
                    self.on_message_sent(command)
                self.timer = time.monotonic()
                logging.info('Sent out: <{}>'.format(cmd_to_send))

    def receive_byte(self):
        try:
            data = self._socket.recv(1)
        except BlockingIOError:
            data = b''
        except OSError as err:
            logging.error('TcpRequestResponseClient.receive_data BlockingIOError: {}'.format(err))
            return
        except ConnectionResetError as err:
            logging.error('TcpRequestResponseClient.receive_data ConnectionResetError: {}'.format(err))
            self.close()
        return data

    def receive_til_delimiter(self):
        data_in = self.receive_byte()
        data_out = data_in
        last_delimiter_byte = chr(self.delimiter[len(self.delimiter)-1]).encode()
        while data_in and data_in != last_delimiter_byte:
            data_in = self.receive_byte()
            data_out += data_in
        return data_out

    def receive_data(self):
        data = self.receive_til_delimiter()
        if data:
            logging.debug('Received: <{}>'.format(data))
            if self.on_message:
                if self.command_que:
                    command = self.command_que.pop(0)
                    data = data.decode()
                    command.reply = data.strip()
                    self.on_message(self, command)
                    if self.on_available:
                        self.on_available()

    def loop(self):
        if not self.run_loop:
            return True
        if self._socket:
            self.receive_data()
            try:
                self.send()
            except BrokenPipeError as err:
                logging.error('TcpRequestResponseClient.loop BrokenPipeError: {}'.format(err))
                self.close()
            except OSError as err:
                logging.error('TcpRequestResponseClient.loop OSError: {}'.format(err))
                self.close()
        elif self._disconnect:
            return False
        else:
            logging.info('Trying to reconnect in 3s')
            time.sleep(3)
            self.reconnect()
        return True

    def close(self):
        logging.info('Closing socket to {} on port {}'.format(self.host, self.port))
        if self._socket:
            self._socket.close()
            self._socket = None
            return
        else:
            logging.info('tcp_request_response.close(): No socket to close')
            return

    def append_to_que(self, command, top_of_que=False):
        """ Add a command to the que, a set reference is a command you would like to put on top of the que"""
        if isinstance(command, Command):
            logging.info('Appending to que: {}'.format(command))
            if top_of_que:
                self.command_que.insert(0, command)
            else:
                self.command_que.append(command)
        else:
            raise ValueError('You can only append a {}.Command'.format(command))

    def test_command(self, test_command):
        self.run_loop = False
        if not isinstance(test_command, bytes):
            test_command = test_command.encode()
        if not test_command.endswith(self.delimiter):
            test_command += self.delimiter
        self.receive_data()
        try:
            self._socket.send(test_command)
        except BrokenPipeError:
            self.reconnect()
        reply = None
        tries = 10
        time.sleep(0.5)
        while not reply:
            try:
                reply = self._socket.recv(1024)
            except BlockingIOError as err:
                logging.error('TcpRequestResponseClient.test_command BlockingIOError: {}'.format(err))
                reply = None
#                reply = b''
            except OSError as err:
                logging.error('TcpRequestResponseClient.test_command OSError: {}'.format(err))
                self.run_loop = True
                return
            if reply:
                if not reply.endswith(b'\n') or reply.endswith(b'\r'):
                    time.sleep(0.2)
                    try:
                        reply += self._socket.recv(1024)
                    except BlockingIOError as err:
                        logging.error('TcpRequestResponseClient.test_command BlockingIOError: {}'.format(err))
                        self.close()
                    except ConnectionResetError as err:
                        logging.error('TcpRequestResponseClient.test_command ConnectionResetError: {}'.format(err))
                        self.close()
                logging.debug('Test response received: <{}>'.format(reply))
                self.run_loop = True
                return reply
            if tries <= 0:
                self.run_loop = True
                logging.debug('No more tries left')
                return reply
            tries -= 1
        self.run_loop = True


if __name__ == '__main__':
    import sys
    logging.basicConfig(level=10, stream=sys.stdout)

    def on_tcprrc_msg(client, command):
        print('Client: {}, message: {}'.format(client.host, command.reply.decode()))

    tcprrc = TcpRequestResponseClient(delimiter=b'\r\n')
    tcprrc.connect()
    tcprrc.on_message = on_tcprrc_msg

    while True:
        print('loop')
        time.sleep(1)
        command = Command(input('Send command: ').encode()) # + tcprrc.delimiter)
        print(command)
        print(tcprrc.test_command(command))
        # tcprrc.append_to_que(command)
        # tcprrc.loop()
