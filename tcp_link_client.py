import sys
import threading
import time
import json
import logging
import socket
import paho.mqtt.client as mqtt
import paho.mqtt.subscribe as subscribe
import paho.mqtt.publish as publish
import tcp_request_response as rr
import lib.links.links as links
import lib.secop.errors as secop_errors
import lib.config_loader.config_loader as config
import argparse
from lib.octopyapp import topics as octopy_topics
from lib.links.request_response_link import RequestResponseLink

config_dict = dict()
try:
    config_dict = config.load('./config')
except KeyError as err:
    print('not able to load config {}'.format(err))
mqtt_config = dict() if not config_dict.get('mqtt') else config_dict.get('mqtt')
NON_METADATA_KEYS = ['get_reference', 'set_reference', 'value_topic', 'get_topic', 'set_topic']
DEFAULT_DELIMITER = '\r\n'
DEFAULT_POLLINTERVALL = 1

STATUS_UPDATE_INTERVAL = 2

TYPE = "socket"

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--host', help="Host name or ip-address of MQTT broker", type=str, default=mqtt_config.get('host'))
arg_parser.add_argument('--port', help="Port of MQTT broker", type=int, default=mqtt_config.get('port') if mqtt_config.get('port') else 1883)
arg_parser.add_argument('--username', help="Username for broker", type=str, default=mqtt_config.get('username'))
arg_parser.add_argument('--password', help="Password for broker", type=str, default=mqtt_config.get('password'))
arg_parser.add_argument('--prefix', help="Prefix", type=str, default=mqtt_config.get('prefix'))
arg_parser.add_argument('--loglevel', help="CRITICAL = 50, ERROR = 40, WARNING = 30, INFO = 20 DEBUG = 10", type=int,
                        default=40)
args = arg_parser.parse_args()
run = True
clients = list()

topics = octopy_topics.get_topics(args.prefix)
storage_get_topic = topics['storage']['get']
storage_set_topic = topics['storage']['set']

category = TYPE+'_request_response'

print('prefix is: {}'.format(args.prefix))
prefix = ''
if args.prefix:
    prefix = args.prefix
    if not prefix.endswith('/'):
        prefix += '/'


OCTOPY_MAIN_TOPIC = prefix + 'octopy'
DATA_SOURCE_MANAGER_TOPIC = OCTOPY_MAIN_TOPIC + '/datasource'
HW_GATEWAY_TOPIC = DATA_SOURCE_MANAGER_TOPIC + '/hwgw'
CONFIG_TOPIC = DATA_SOURCE_MANAGER_TOPIC + '/' + TYPE + '/config'
logging.basicConfig(level=args.loglevel, stream=sys.stdout)

if args.username:
    auth = {'username': args.username, 'password': args.password}
else:
    auth = None


class TcpLinkClient(threading.Thread):
    """The link client connect to a client/server based interface using links from the octopus library to
    determine what serial command should be used"""
    def __init__(self, host=rr.DEFAULT_IP, port=rr.DEFAULT_PORT, mqtt_host=args.host, mqtt_port=args.port,
                 delimiter=DEFAULT_DELIMITER, pollintervall=DEFAULT_POLLINTERVALL, **kwargs):
        self._init_time = time.time()
        self.category = 'tcp_request_response'
        hostname = socket.gethostname()
        logging.debug('hostname is: {}'.format(hostname))
        self.ip = socket.gethostbyname(hostname)
        self.hw_host = host if not host == 'localhost' else self.ip
        if self.hw_host == '':
            self.hw_host = self.ip
        logging.debug('host is: {}'.format(self.hw_host))
        self.hw_port = port
        if pollintervall == '':
            pollintervall = DEFAULT_POLLINTERVALL
        self.pollintervall = pollintervall
        if delimiter == '':
            delimiter = '\r\n'
        self.delimiter = delimiter.encode()
        self.mqtt_host = mqtt_host
        self.mqtt_port = mqtt_port
        self.linklist = links.LinkList(RequestResponseLink)
        self.command_separator = ' ' if not kwargs.get('separator') else kwargs.get('separator')
        if isinstance(self.command_separator, bytes):
            self.command_separator = self.command_separator.decode()
        self.current_link = None
        self.caller_id = kwargs.get('call id')
        self.m_client = mqtt.Client()
        self.id = self.hw_host + ':' + str(self.hw_port)
        self.edit_channel_topic = DATA_SOURCE_MANAGER_TOPIC + '/channels/set'
        self.delete_channel_topic = DATA_SOURCE_MANAGER_TOPIC + '/channels/remove'
        self.command_in_topic = HW_GATEWAY_TOPIC + '/' + TYPE + '/command/in/' + self.id
        self.command_out_topic = HW_GATEWAY_TOPIC + '/' + TYPE + '/command/out/' + self.id
        self.config_topic = CONFIG_TOPIC
        self.status_topic = HW_GATEWAY_TOPIC + '/status/' + self.id
        self.link_topic = HW_GATEWAY_TOPIC + '/links/' + self.id
        self.remove_gw_topic = DATA_SOURCE_MANAGER_TOPIC + '/sources/remove'
        self.storage_get_topic = kwargs.get('storage_get_topic')
        logging.debug('storage_get_topic is: {}'.format(self.storage_get_topic))
        self.storage_set_topic = kwargs.get('storage_set_topic')
        logging.debug('storage_set_topic is: {}'.format(self.storage_set_topic))
        self._will = {'topic': self.link_topic, 'payload': json.dumps('[]'), 'qos': 1, 'retain': False}
        self.m_client.will_set(**self._will)
        self.m_client.reconnect_delay_set(min_delay=1, max_delay=30)
        if auth:
            self.m_client.username_pw_set(**auth)
        self.logging = logging.getLogger('tcp_link_client')
        threading.Thread.__init__(self)
        self.loops = 0
        self.added = False

    def run(self):
        try:
            self.rrc = rr.TcpRequestResponseClient(pollintervall=self.pollintervall, delimiter=self.delimiter)
            if self.rrc.connect(self.hw_host, self.hw_port):
                self.rrc.on_message = self.on_sc_message
                self.rrc.on_message_sent = self.on_sc_message_sent
                self.rrc.on_available = self.on_sc_available
                self.m_client.on_connect = self.on_mqtt_connect
            else:
                return
            try:
                self.m_client.connect(self.mqtt_host, self.mqtt_port, 60)
            except ConnectionRefusedError as err:
                logging.error('MQTT ' + str(err))
                self.cleanup()
                sys.exit(1)
            self.loops = 0
            self.publish_status()
            self.m_client.loop()
            connected = True
            tick = 0.1
            last_status_update = time.time() - STATUS_UPDATE_INTERVAL
            # last_link_update = time.time() - LINK_PUBLISH_INTERVAL
            last_mqtt_misc = time.time()
            while connected:
                self.m_client.loop(tick)
                current_time = time.time()
                if current_time > last_status_update + STATUS_UPDATE_INTERVAL:
                    # Publish datasource status regularly
                    last_status_update = current_time
                    self.publish_status()
                if current_time > last_mqtt_misc + 2:
                    # Let the MQTT library do its housekeeping regularly
                    last_mqtt_misc = current_time
                    self.m_client.loop_misc()
                connected = self.rrc.loop()
        except RuntimeError as err:
            logging.error(err)
            self.cleanup()
            return
        except KeyboardInterrupt as err:
            logging.error(err)
            self.cleanup()
            return
        self.cleanup()

    def status_dict(self):
        ret = dict()
        ret["datasource id"] = self.id
        ret["type"] = "socket"
        ret["uptime"] = int(time.time()-self._init_time)
        ret["status"] = "connected"
        ret["delimiter"] = self.delimiter.decode()
        ret["host"] = self.hw_host
        ret["port"] = self.hw_port
        ret["pollintervall"] = self.pollintervall
        ret["links"] = self.linklist.linklist_as_list()
        ret["config topic"] = self.config_topic
        ret["command topic"] = self.command_in_topic
        return ret

    def storage_dict(self):
        ret = dict()
        ret["datasource id"] = self.id
        ret["type"] = TYPE
        ret["delimiter"] = self.delimiter.decode()
        ret["host"] = self.hw_host
        ret["port"] = self.hw_port
        ret["pollintervall"] = self.pollintervall
        ret["config topic"] = self.config_topic
        return ret

    def cleanup(self):
        logging.debug('cleanup, exiting')
        self.m_client.publish(topic=self.status_topic, qos=1, payload=json.dumps(''), retain=True)
        self.m_client.publish(topic=self.link_topic, qos=1, payload=json.dumps([]), retain=False)
        time.sleep(0.5)
        self.rrc.disconnect()
        self.m_client.loop_stop()
        self.m_client.disconnect()
        return

    def on_mqtt_connect(self, client, userdata, flags, rc):
        """When it connects to the broker it starts to listen on [topic]/[port]/in for sending commands like you would
        in a terminal window.
        and on [topic]/[port]/add_link for creating a basic octopus link in json format:
        {"get_topic":"[topic_for_triggering_a_get_request]",
        "get_reference":"[command_the_client_would_use_to_trigger_a_read]",
        "set_topic":"[topic_for_triggering_a_set_request]",
        "set_reference":"[command_the_client_would_use_to_trigger_a_write]"}

        eg(julabo):
        {"get_topic":"temp","get_reference":"IN_PV_00","set_topic":"setpoint","set_reference":"OUT_SP_00"}
        """
        logging.info("Connected with result code " + str(rc))
        self.m_client.subscribe([(self.command_in_topic, 1), (self.delete_channel_topic, 1), (self.remove_gw_topic, 1), (self.edit_channel_topic, 1)])
        self.m_client.message_callback_add(self.command_in_topic, self.on_mqtt_nolink_message)
        self.m_client.message_callback_add(self.delete_channel_topic, self.on_mqtt_remove_link_message)
        self.m_client.message_callback_add(self.remove_gw_topic, self.on_mqtt_stop_message)
        self.m_client.message_callback_add(self.edit_channel_topic, self.on_mqtt_edit_link_message)
        logging.debug('Added callback for receiving channels on: {}'.format(self.edit_channel_topic))
        time.sleep(0.1)
        self.load_links()

    def on_mqtt_nolink_message(self, client, userdata, msg):
        """Callback used for /in topic"""
        logging.debug(TYPE + 'LinkClient.on_mqtt_nolink_message')
        try:
            pl = json.loads(msg.payload)
        except json.decoder.JSONDecodeError:
            pl = msg.payload.decode()
            print(pl)
        if not isinstance(pl, dict):
            pl = {'in': pl}
        cmd = pl.get('in')
        if cmd:
            ret = self.rrc.test_command(str(cmd).encode() + self.delimiter)
            print(ret)
            if ret:
                pl['out'] = ret.decode()
            else:
                pl['error'] = 'no reply'
        else:
            pl['error'] = 'no command'
        try:
            out = json.dumps(pl)
        except Exception as err:
            print(err)
            return
        self.m_client.publish(topic=self.command_out_topic, qos=1, payload=out)

    def on_mqtt_remove_link_message(self, client, userdata, msg): #TODO fix multiple links, remove callbacks
        """Callback used for /remove_link topic"""
        logging.debug(TYPE + 'LinkClient.on_mqtt_remove_link_message: {}'.format(msg.payload))
        message_list = list()
        message = str(msg.payload)
        try:
            message = json.loads(msg.payload.decode())
        except json.JSONDecodeError as err:
            logging.error('In ' + TYPE + '_link_client payload: {}, error: {}'.format(msg.payload, err))
        if message:
            if not isinstance(message, list):
                message_list.append(message)
            else:
                message_list = message
            for value_topic in message_list:
                if isinstance(value_topic, dict):
                    value_topic = value_topic.get('channel id')
                try:
                    self.linklist.delete(value_topic)
                except KeyError as err:
                    logging.error(err)
                    return
                logging.info('removed link: {}'.format(value_topic))
        else:
            logging.error('Can not remove channel, empty payload: {}'.format(msg.payload))
        time.sleep(0.1)
        self.publish_links()

    def add_mqtt_callback(self, lnk):
        if lnk.get_topic:
            self.m_client.subscribe(lnk.get_topic)
            self.m_client.message_callback_add(lnk.get_topic, self.get_request)
        if lnk.set_topic:
            self.m_client.subscribe(lnk.set_topic)
            self.m_client.message_callback_add(lnk.set_topic, self.set_request)

    def update_mqtt_callback(self, new_lnk, old_lnk):
        if new_lnk.set_topic:
            if not old_lnk.set_topic:
                self.m_client.subscribe(new_lnk.set_topic)
                self.m_client.message_callback_add(new_lnk.set_topic, self.set_request)
            elif old_lnk.set_topic != new_lnk.set_topic:
                self.m_client.message_callback_remove(old_lnk.set_topic)
                self.m_client.unsubscribe(old_lnk.set_topic)
                self.m_client.subscribe(new_lnk.set_topic)
                self.m_client.message_callback_add(new_lnk.set_topic, self.set_request)
        if new_lnk.get_topic:
            if not old_lnk.get_topic:
                self.m_client.subscribe(new_lnk.get_topic)
                self.m_client.message_callback_add(new_lnk.get_topic, self.get_request)
            elif old_lnk.get_topic != new_lnk.get_topic:
                self.m_client.message_callback_remove(old_lnk.get_topic)
                self.m_client.unsubscribe(old_lnk.get_topic)
                self.m_client.subscribe(new_lnk.get_topic)
                self.m_client.message_callback_add(new_lnk.get_topic, self.get_request)

    def on_mqtt_edit_link_message(self, client, userdata, msg):
        """Callback used for /edit_channel topic"""
        self.added = False
        lnk = None
        payload = json.loads(msg.payload)
        logging.debug(TYPE + 'LinkClient.on_mqtt_edit_link_message')
        message_list = payload
        if isinstance(payload, dict):
            message_list = payload.get('value')
            if not message_list:
                message_list = [payload]
        elif not isinstance(payload, list):
            message_list = [payload]
        logging.info('Editing channels: {}\nfrom payload: {}'.format(message_list, payload))
        for link_dict in message_list:
            logging.info('Editing channel: {}\n'.format(link_dict))
            if isinstance(link_dict, dict):
                logging.info('Editing channel...')
                if link_dict.get('datasource id') != self.id:
                    return
                v_t = link_dict.get('value topic')
                if not v_t:
                    logging.error('value topic is needed')
                    return
                lnk = self.linklist.find_by_value_topic(v_t)
                if not lnk:
                    reply = None
                    g_r = link_dict.get('get reference')
                    if g_r:
                        reply = self.rrc.test_command(g_r.encode())
                        logging.debug('Command {} returned: {}'.format(g_r.encode(), reply))
                    if reply is None:
                        logging.warning('There is no reply from the hardware on command: {}'.format(g_r))
                        return
                    lnk = self.linklist.add_link(link_dict)
                    try:
                        reply = lnk.parse_reply(reply) # TODO publish
                    except secop_errors.SecopException as err:
                        logging.error(err)
                    logging.debug('Testing of command success received: {}, datainfo: {}'.format(reply, lnk.datainfo))
                    self.add_mqtt_callback(lnk)
                    logging.info('Added link: ' + lnk.value_topic)
                    self.added = True
                else:
                    new_lnk = RequestResponseLink(link_dict)
                    self.update_mqtt_callback(new_lnk, lnk)
                    lnk.update_from_dict(link_dict)
        if self.added:
            self.store_links()
            if lnk:
                self.rrc.append_to_que(rr.Command(lnk.get_reference.encode() + self.delimiter, lnk))
        self.publish_links()

    def on_mqtt_stop_message(self, client, userdata, msg):
        """Callback used for /stop topic"""
        logging.debug(TYPE + 'LinkClient.on_mqtt_stop_message')
        try:
            message = json.loads(msg.payload)
        except json.JSONDecodeError as err:
            logging.error(err)
            return
        logging.debug('Stop message: {} and id is: {}'.format(message, self.id))
        if isinstance(message, dict):
            if message.get('datasource id') == self.id:
                self.cleanup()
        elif isinstance(message, str):
            if message == self.id:
                self.cleanup()

    def publish_status(self):
        status_json = json.dumps(self.status_dict())
        logging.debug("publishing: {} on topic: {}".format(status_json, self.status_topic))
        self.m_client.publish(topic=self.status_topic, qos=1, payload=status_json)

    def publish_links(self):
        try:
            lnks = self.linklist.linklist_as_json()
        except TypeError as err:
            logging.error(TYPE + 'LinkClient.publish_links: {}'.format(err))
            return
        logging.debug('publishing links on {}: {}'.format(self.link_topic, lnks))
        self.m_client.publish(topic=self.link_topic, payload=lnks, qos=1, retain=False)

    def store_links(self):
        """ Stores all links in storage manager """
        logging.debug('Attempting to store links to storage manager')
        if self.storage_set_topic:
            try:
                lnks = self.linklist.linklist_as_list()
            except TypeError as err:
                logging.error(TYPE + 'LinkClient.store_links: {}'.format(err))
                return
            logging.info('Storing links on {} as {}: {}'.format(self.storage_set_topic, self.id, json.dumps(lnks, indent=4)))
            payload_to_store = {'value': lnks, 'category': self.category, 'key': self.id}
            self.m_client.publish(topic=self.storage_set_topic, payload=json.dumps(payload_to_store), qos=1)

    def load_links(self):
        """ Publishes to the storage manager and tells it to send all stored links to the add channels topic,
         which in turn calls the on_mqtt_edit_link_message-function thus adding the stored links """
        if self.storage_get_topic:
            logging.info('loading links from {}'.format(self.storage_get_topic))
            payload = {'return topic': self.edit_channel_topic, 'category': self.category, 'key': self.id}
            self.m_client.publish(topic=self.storage_get_topic, payload=json.dumps(payload), qos=1)

    def get_request(self, client, userdata, msg):
        logging.info(TYPE + 'LinkClient.get_request: get request on topic: {}, message: {}'.format(msg.topic, msg.payload))
        lnk = self.linklist.find_by_get_topic(msg.topic)
        if lnk:
            self.rrc.append_to_que(rr.Command(lnk.get_reference.encode() + self.delimiter, lnk))

    def set_request(self, client, userdata, msg):
        logging.info(TYPE + 'LinkClient.set_request:{} set request on topic: {}'.format(msg.payload, msg.topic))
        message = b''
        lnk = self.linklist.find_by_set_topic(msg.topic)
        if lnk:
            try:
                json_message = json.loads(msg.payload.decode())
            except json.JSONDecodeError as err:
                logging.error('Not able to decode json: {}'.format(err))
                return
            try:
                message = list(json_message)[0]
            except IndexError as err:
                logging.error('{} is not a valid array'.format(msg.payload))
            except TypeError as err:
                logging.error('{} is not a valid array'.format(msg.payload))
            cmd = lnk.synthesize_command(message, self.delimiter)
            logging.info('Appending command {} to que'.format(cmd))
            self.rrc.append_to_que(rr.Command(cmd, lnk, no_reply=True), top_of_que=True)
        else:
            logging.error(TYPE + 'LinkClient.set_request: Link not found')

    def on_sc_message(self, client, command):
        message = command.reply
        if self.current_link:
            logging.debug(TYPE + 'LinkClient.on_sc_message: {} for link {}'.format(message, self.current_link.value_topic))
            val = self.current_link.parse_reply(message)
            if val is not None:
                try:
                    mesg = [val, {'t': time.time()}]
                    logging.debug('Publishing {}'.format(mesg))
                    self.m_client.publish(self.current_link.value_topic, json.dumps(mesg), qos=1)
                except ValueError as err:
                    logging.error('on_sc_message error: ' + str(err))
            else:
                logging.warning('No value in message')
            self.m_client.publish(self.current_link.value_topic + '/str', command.reply, qos=1)
        else:
            logging.debug('Empty message')

    def on_sc_available(self):
        logging.debug(TYPE + 'LinkClient.on_sc_available')
        if len(self.linklist) > 0:
            nxt_lnk = self.linklist.next_prioritized()
            if isinstance(nxt_lnk, RequestResponseLink):
                command = nxt_lnk.get_reference
                if command:
                    cmd = nxt_lnk.get_reference.encode('utf-8') + self.delimiter
                    self.rrc.append_to_que(rr.Command(cmd, nxt_lnk))
                else:
                    logging.debug('Warning, trying to enqueue a link without a command(get_reference). '
                                  'Link is useless to this GW removing Link')
                    self.linklist.delete(nxt_lnk)
            else:
                logging.warning('Warning links.LinkList.next_prioritized did not return a link')
        else:
            logging.warning('No links')

    def on_sc_message_sent(self, command):
        logging.debug(TYPE + 'LinkClient.on_sc_message_sent({})'.format(str(command)))
        sent_message = command.reply
        if isinstance(sent_message, bytes):
            sent_message = sent_message.decode()
        reference = sent_message.split(self.command_separator)
        current_link = command.link
        if current_link:
            self.current_link = current_link
            logging.debug('Sent message is: {} and {} is the reference.\nfind_by_get_reference yields: {}'
                          .format(sent_message, reference[0], self.current_link))
        else:
            logging.warning('Link not found, the sent message is: {} and {} is the reference.\nfind_by_get_reference '
                            'yields: {}'.format(sent_message, reference[0], self.current_link))

    def on_sc_no_reply(self, command):
        if command.lnk:
            if command.command == command.lnk.set_topic:
                command.lnk.replay_on_set = False


def new_instance(settings_list):
    if isinstance(settings_list, dict):
        if 'value' in settings_list.keys():
            settings_list = settings_list.get('value')
        else:
            settings_list = [settings_list]
    elif not isinstance(settings_list, list):
        return
    for settings in settings_list:
        if isinstance(settings, dict):
            duplicate_client = False
            for client in clients:
                if client.hw_host == settings.get('host'): # or rr.DEFAULT_IP:
                    port = settings.get('port')
                    if isinstance(port, str):
                        settings['port'] = int(settings.get('port'))
                    if client.hw_port == settings.get('port'):
                        duplicate_client = client.is_alive()
            if duplicate_client:
                logging.warning('client duplication host: {} and {} port: {} and {}'.format(settings.get('host'), client.hw_host, settings.get('port'), client.hw_port))
            else:

                logging.debug('Main tread: Starting new thread...')
                try:
                    settings['storage_get_topic'] = storage_get_topic
                    settings['storage_set_topic'] = storage_set_topic
                    clients.append(TcpLinkClient(**settings))
                    clients[len(clients) - 1].start()
                    logging.info('Main tread: New thread started')
                except TypeError as err:
                    logging.error(str(err))
        else:
            logging.error('Settings for a new instance should be a json object')
        time.sleep(1)


def store_datasources(value, category, key):
    logging.debug('Storing gateways to storage manager')
    if storage_set_topic:
        payload = {'value': value, 'category': category, 'key': key}
        logging.info('Storing datasource: {}: {}'.format(storage_set_topic, json.dumps(payload, indent=4)))
        publish.single(topic=storage_set_topic, payload=json.dumps(payload), hostname=args.host, port=args.port,
                       auth=auth, qos=1)


def publish_once(topic, delay=0.1, message=''):
    time.sleep(delay)
    print('publishing: {}'.format(message))
    publish.single(topic=topic, hostname=args.host, port=args.port, auth=auth, retain=False, qos=1, payload=message)


if __name__ == '__main__':
    status_topic = HW_GATEWAY_TOPIC + '/' + TYPE + '/status'
    clients_info = list()
    tries = 10
    print('loglevel: {}'.format(args.loglevel))
    try:
        while tries > 0:
            try_again = False
            now = time.time()
            try:
                publish.single(topic=status_topic, hostname=args.host,
                               port=args.port, auth=auth, retain=False, qos=1, payload=
                               json.dumps({"t": int(now), "status": "connected", "instances": clients_info}))
            except socket.gaierror as err:
                logging.error(err)
                tries -= 1
                time.sleep(2)
            except ConnectionRefusedError as err:
                logging.error(err)
                tries -= 1
                time.sleep(2)
            else:
                break
            logging.warning('Retrying to connect to broker, {} times left'.format(tries))
    except KeyboardInterrupt:
        exit(0)
    logging.info('Connected to broker')
    logging.debug('Attempting to fetch gateway configurations from storage manager')
    logging.info('Listening for config on: {}'.format(CONFIG_TOPIC))
    payload = json.dumps({'return topic': CONFIG_TOPIC, 'category': category, 'key': 'instances'})
    t = threading.Thread(target=publish_once, args=(storage_get_topic, 2, payload))
    t.start()
    while True:
        msg = None
        try:
            msg = subscribe.simple(CONFIG_TOPIC, hostname=args.host, port=args.port, auth=auth,
                                   will={'topic': status_topic, 'payload': '', 'qos': 1, 'retain': False})
        except KeyboardInterrupt:
            logging.warning('Keyboard interrupt, stopping all instances')
            for client in clients:
                topic = DATA_SOURCE_MANAGER_TOPIC + '/sources/remove'
                logging.debug('stopping instance on {}'.format(topic))
                publish.single(topic=topic, hostname=args.host, port=args.port,
                               auth=auth, qos=1, payload=json.dumps({'datasource id': client.id}))
            time.sleep(1)
            all_stopped = True
            for client in clients:
                while client.is_alive():
                    logging.warning('instance on {} port {}, is NOT stopped, waiting'.format(client.hw_host, client.hw_port))
                    topic = DATA_SOURCE_MANAGER_TOPIC + '/sources/remove'
                    publish.single(topic=topic, hostname=args.host, port=args.port,
                                   auth=auth, qos=1, payload=json.dumps({'datasource id': client.id}))
                    time.sleep(1)
                logging.warning('instance on {} port {}, is stopped'.format(client.hw_host, client.hw_port))
            exit(0)
        if msg:
            logging.info('Config message received')
            try:
                setting = json.loads(msg.payload)
            except json.decoder.JSONDecodeError as err:
                logging.error(str(err))
            except TypeError as err:
                logging.error(str(err))
            else:
                new_instance(setting)
            clients_info = []
            for client in clients:
                if client.is_alive():
                    clients_info.append(client.storage_dict())
                else:
                    clients.remove(client)
            store_datasources(clients_info, category=category, key='instances')
            now = time.time()
            publish.single(status_topic, hostname=args.host,
                           port=args.port, auth=auth, retain=False, qos=1, payload=
                           json.dumps({"t": int(now), "status": "connected", "instances": clients_info}))



